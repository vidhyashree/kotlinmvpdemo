package com.vidhyashree.assignment.kotlinmvpdemo.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.vidhyashreeappaji.mvpdemo.R
import com.vidhyashree.assignment.kotlinmvpdemo.model.RowItem

class RowItemAdapter(private val mContext: Context, rowList: ArrayList<RowItem>) : RecyclerView.Adapter<RowItemAdapter.ViewHolder>() {

    private val rowList: ArrayList<RowItem> = rowList

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.row_item, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        val rowItem = rowList[position]
        if (rowItem.description.isNullOrEmpty() && rowItem.title.isNullOrEmpty() && rowItem.imageHref.isNullOrEmpty()) {
            rowList.remove(rowItem)
            /*  notifyItemRemoved(position)
              notifyItemRangeChanged(position, itemCount);*/
        }

        //loading the data
        else {
            viewHolder.mTitle.text = rowItem.title
            viewHolder.mDescription.text = rowItem.description
            //image loading using Picasso
            Glide.with(mContext).load(rowList[position].imageHref).into(viewHolder.mImage)
        }
    }

    override fun getItemCount(): Int {
        return rowList.size
    }

    //View holder class for individual views
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal val mTitle: TextView = itemView.findViewById(R.id.row_title)
        internal val mDescription: TextView = itemView.findViewById(R.id.row_description)
        internal val mImage: ImageView = itemView.findViewById(R.id.row_image)

        init {
            //row item clicklistener for onclick events
            itemView.setOnClickListener { Toast.makeText(mContext, rowList[adapterPosition].title, Toast.LENGTH_SHORT).show() }
        }
    }
}
