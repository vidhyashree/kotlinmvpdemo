package com.vidhyashree.assignment.kotlinmvpdemo.di.component

import android.app.Application
import android.content.Context
import com.vidhyashree.assignment.kotlinmvpdemo.MyApplication
import com.vidhyashree.assignment.kotlinmvpdemo.di.module.AppModule
import com.vidhyashree.assignment.kotlinmvpdemo.di.module.ContextModule
import dagger.Component
import javax.inject.Singleton

//This is the App level Component. The fields would be accessible throughout the application.
@Singleton
@Component(modules = [AppModule::class, ContextModule::class])
interface AppComponent {

    val context: Context

    val application: Application
    fun inject(myApplication: MyApplication)
}

