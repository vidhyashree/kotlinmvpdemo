package com.vidhyashree.assignment.kotlinmvpdemo.view

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.vidhyashree.assignment.kotlinmvpdemo.MyApplication
import com.example.vidhyashreeappaji.mvpdemo.R
import com.vidhyashree.assignment.kotlinmvpdemo.model.ListItem
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.presenter.MainPresenter
import com.vidhyashree.assignment.kotlinmvpdemo.adapter.RowItemAdapter
import com.vidhyashree.assignment.kotlinmvpdemo.di.component.DaggerFragmentComponent
import com.vidhyashree.assignment.kotlinmvpdemo.di.module.FragmentModule
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.MVPView
import com.vidhyashree.assignment.kotlinmvpdemo.utils.NetworkConnectivityReceiver
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class ListViewFragment : Fragment(), MVPView, NetworkConnectivityReceiver.NetworkConnectivityListener {

    @BindView(R.id.recyclerview)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.progress_bar)
    lateinit var progressBar: ProgressBar

    @BindView(R.id.swipe_container)
    lateinit var swipeContainer: SwipeRefreshLayout

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var mContext: Context

    private lateinit var mListener: MyListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_listview, container, false)
        //Butterknife to bind the view
        ButterKnife.bind(this, rootView)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //initialize swipe to refresh layout
        initRefreshLayout()
        //Injecting the presenter without intantiating it
        DaggerFragmentComponent.builder()
                .appComponent(activity?.let { MyApplication[it].component() })
                .fragmentModule(FragmentModule(this))
                .build()
                .inject(this)

        //call to fetch data from server
        presenter.fetchDatafromServer()

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mListener = context as MyListener
    }

    private fun initRefreshLayout() {
        swipeContainer.setOnRefreshListener {
            //code to refresh the list.
            swipeContainer.isRefreshing = false
            presenter.onRefresh()
        }
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light)
    }

    override fun setItems(listItem: ListItem) {
        mListener.setTitle(listItem.title)
        val recyclerViewAdapter = RowItemAdapter(mContext, listItem.rows)
        recyclerView.layoutManager = LinearLayoutManager(mContext)
        recyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.itemCount
    }


    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onFailure(t: Throwable) {
        Toast.makeText(activity, getString(R.string.error_message) + t.message, Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        super.onResume()
        //set listener for network change
        activity?.let { MyApplication[it].setConnectivityListener(this) }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        //handle on network change
        if (!isConnected) {
            Snackbar.make(activity!!.findViewById(R.id.snackbar_view), "Internet Unavailable!", Snackbar.LENGTH_INDEFINITE).show()
        } else {
            Snackbar.make(activity!!.findViewById(R.id.snackbar_view), "Internet Available!", Snackbar.LENGTH_LONG).show()
            presenter.onRefresh()
        }
    }

    interface MyListener {
        fun setTitle(title: String)
    }
}
