package com.vidhyashree.assignment.kotlinmvpdemo.mvp.presenter

interface MainPresenter {

    fun onRefresh()

    fun fetchDatafromServer()

    fun onDestroy()
}