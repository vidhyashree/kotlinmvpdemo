package com.vidhyashree.assignment.kotlinmvpdemo.di.module

import android.content.Context
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.presenter.MainPresenterImpl
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor.Interactor
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor.InteractorImpl
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.MVPView
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.presenter.MainPresenter

import dagger.Module
import dagger.Provides

//This provides an instance of the MainPresenterImpl class.
//It takes the View’s interface  as it’s constructor argument.
@Module
class FragmentModule(private val mvpView: MVPView) {

    @Provides
    fun provideView(): MVPView {
        return mvpView
    }

    @Provides
    fun providePresenter(view: MVPView, interactor: Interactor): MainPresenter {
        return MainPresenterImpl(view, interactor)
    }

    @Provides
    fun provideInteractor(context: Context): Interactor {
        return InteractorImpl(context)
    }


}
