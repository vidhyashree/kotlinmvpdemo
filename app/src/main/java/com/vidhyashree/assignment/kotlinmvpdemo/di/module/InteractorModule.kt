package com.vidhyashree.assignment.kotlinmvpdemo.di.module

import android.content.Context
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor.Interactor
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor.InteractorImpl
import dagger.Module
import dagger.Provides

//This provides an instance of Model(InteractorImpl) class for us.
@Module
class InteractorModule {

    @Provides
    internal fun provideInteractor(context: Context): Interactor {
        return InteractorImpl(context)
    }
}
