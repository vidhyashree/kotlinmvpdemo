package com.vidhyashree.assignment.kotlinmvpdemo.model

import com.vidhyashree.assignment.kotlinmvpdemo.model.RowItem
import java.util.ArrayList

/*Model class for ListItems*/

class ListItem {

    var id: Int = 0
    lateinit var title: String
    lateinit var rows: ArrayList<RowItem>
}
