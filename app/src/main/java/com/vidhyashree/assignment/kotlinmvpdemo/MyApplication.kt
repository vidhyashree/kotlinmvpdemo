package com.vidhyashree.assignment.kotlinmvpdemo

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import com.vidhyashree.assignment.kotlinmvpdemo.di.component.AppComponent
import com.vidhyashree.assignment.kotlinmvpdemo.di.component.DaggerAppComponent
import com.vidhyashree.assignment.kotlinmvpdemo.di.module.AppModule
import com.vidhyashree.assignment.kotlinmvpdemo.di.module.ContextModule
import com.vidhyashree.assignment.kotlinmvpdemo.utils.NetworkConnectivityReceiver

class MyApplication : Application() {
    private lateinit var myReceiver: NetworkConnectivityReceiver
    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()


        appContext = this
        //initialize appComponent
        component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .contextModule(ContextModule(this))
                .build()

        component.inject(this)

        registerReceiver()
    }

    fun component(): AppComponent? {
        return component
    }

    private fun getMyReceiver(): NetworkConnectivityReceiver {

        myReceiver = NetworkConnectivityReceiver()

        return myReceiver
    }

    fun setConnectivityListener(listener: NetworkConnectivityReceiver.NetworkConnectivityListener) {
        NetworkConnectivityReceiver.connectivityReceiverListener = listener
    }

    /*Explicitly register receiver. Since declaring BraoadcastReceiver in manifest
     doesn't work in android OREO and above*/
    private fun registerReceiver() {
        val filter = IntentFilter()
        @Suppress("DEPRECATION")
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)
        registerReceiver(getMyReceiver(), filter)
    }

    companion object {

        @get:Synchronized
        var appContext: Context? = null
            private set

        operator fun get(context: Context): MyApplication {
            return context.applicationContext as MyApplication
        }
    }
}
