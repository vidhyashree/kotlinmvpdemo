package com.vidhyashree.assignment.kotlinmvpdemo.mvp.presenter

import com.vidhyashree.assignment.kotlinmvpdemo.model.ListItem
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor.Interactor
import com.vidhyashree.assignment.kotlinmvpdemo.mvp.MVPView

//Presenter implementation
class MainPresenterImpl//Constructor which takes view and model interfaces as parameters
(private var mvpView: MVPView?, private val interactor: Interactor) : MainPresenter, Interactor.OnFinishedListener {


    override fun onDestroy() {
        mvpView = null
    }

    override fun onRefresh() {
        if (mvpView != null) {
            mvpView!!.showProgress()
        }
        interactor.fetchNewDataFromServer(this)

    }

    override fun onFinished(item: ListItem) {

        if (mvpView != null) {
            mvpView!!.setItems(item)
            mvpView!!.hideProgress()
        }

    }

    override fun onFailure(t: Throwable) {
        if (mvpView != null) {
            mvpView!!.onFailure(t)
            mvpView!!.hideProgress()
        }

    }

    override fun fetchDatafromServer() {
        interactor.fetchNewDataFromServer(this)
    }
}
