package com.vidhyashree.assignment.kotlinmvpdemo.retrofit

import com.vidhyashree.assignment.kotlinmvpdemo.model.ListItem
import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {
    @get:GET("s/2iodh4vg0eortkl/facts.json")
    val allItems: Call<ListItem>
}
