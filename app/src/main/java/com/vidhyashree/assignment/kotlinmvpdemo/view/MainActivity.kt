package com.vidhyashree.assignment.kotlinmvpdemo.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.example.vidhyashreeappaji.mvpdemo.R
import com.vidhyashree.assignment.kotlinmvpdemo.view.ListViewFragment.MyListener

class MainActivity : AppCompatActivity(), MyListener {

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Butterknife to bind view
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_container, ListViewFragment()).commit()
        }
    }

    override fun setTitle(title: String) {
        toolbar.title = title
    }
}
