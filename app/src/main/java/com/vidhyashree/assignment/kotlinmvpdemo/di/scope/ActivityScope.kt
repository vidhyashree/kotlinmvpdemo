package com.vidhyashree.assignment.kotlinmvpdemo.di.scope

import javax.inject.Scope

// ActivityScope for components
@Scope
@kotlin.annotation.Retention
annotation class ActivityScope
