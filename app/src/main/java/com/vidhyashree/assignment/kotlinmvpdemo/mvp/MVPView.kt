package com.vidhyashree.assignment.kotlinmvpdemo.mvp

import com.vidhyashree.assignment.kotlinmvpdemo.model.ListItem

interface MVPView {

    fun setItems(listItem: ListItem)

    fun showProgress()

    fun hideProgress()

    fun onFailure(t: Throwable)

}
