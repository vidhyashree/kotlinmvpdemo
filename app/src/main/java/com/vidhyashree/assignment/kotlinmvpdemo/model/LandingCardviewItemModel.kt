package com.vidhyashree.assignment.kotlinmvpdemo.model

import com.google.gson.annotations.SerializedName

data class LandingCardviewItemModel(

        @SerializedName("Type")
        val type: String,

        @SerializedName("OrderCount")
        val orderCount: String,

        @SerializedName("Image")
        val image: String,

        @SerializedName("isOrder")
        val isOrder: Boolean,

        @SerializedName("BackgroundColor")
        val backgroundColor: String,

        @SerializedName("ClickAction")
        val clickAction: String
)
