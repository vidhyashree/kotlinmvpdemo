package com.vidhyashree.assignment.kotlinmvpdemo.di.module

import android.content.Context

import dagger.Module
import dagger.Provides

//Provide the application context
@Module
class ContextModule(private val context: Context) {

    @Provides
    fun provideContext(): Context {
        return context
    }
}
