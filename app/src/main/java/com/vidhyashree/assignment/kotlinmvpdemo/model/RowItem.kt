package com.vidhyashree.assignment.kotlinmvpdemo.model

import com.google.gson.annotations.SerializedName

class RowItem {

    @SerializedName("title")
    var title: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("imageHref")
    var imageHref: String = ""
}
