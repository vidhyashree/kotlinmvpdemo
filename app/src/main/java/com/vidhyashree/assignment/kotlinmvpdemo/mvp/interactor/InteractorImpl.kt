package com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor

import android.content.Context
import com.vidhyashree.assignment.kotlinmvpdemo.model.ListItem
import com.vidhyashree.assignment.kotlinmvpdemo.retrofit.APIInterface
import com.vidhyashree.assignment.kotlinmvpdemo.retrofit.RetroFitAPIClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InteractorImpl(private var context: Context) : Interactor {


    //fetch new data from the server
    override fun fetchNewDataFromServer(onFinishedListener: Interactor.OnFinishedListener) {
        //Retrofit call to fetch data
        val responseObj = RetroFitAPIClient.getRetrofitInstance(context)!!.create<APIInterface>(APIInterface::class.java).allItems
        responseObj.enqueue(object : Callback<ListItem> {
            override fun onResponse(call: Call<ListItem>, response: Response<ListItem>) {
                if (response.isSuccessful) {
                    if (response.body() != null) onFinishedListener.onFinished(response.body()!!)
                }
            }

            override fun onFailure(call: Call<ListItem>, t: Throwable) {
                onFinishedListener.onFailure(t)
            }
        })
    }

}
