package com.vidhyashree.assignment.kotlinmvpdemo.adapter

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import java.util.ArrayList

import android.support.v7.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vidhyashree.assignment.kotlinmvpdemo.R
import com.vidhyashree.assignment.kotlinmvpdemo.model.LandingCardviewItemModel

class LandingAdapter : RecyclerView.Adapter<LandingAdapter.ViewHolder>() {


    private var rowList: List<LandingCardviewItemModel>? = null
    private lateinit var mContext: Context


    fun LandingCardviewAdapter(context: Context, rowList: ArrayList<LandingCardviewItemModel>) {
        this.rowList = rowList
        mContext = context
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val mType: TextView = itemView.findViewById(R.id.row_title)
        val mOrderCount: TextView = itemView.findViewById(R.id.row_description)
        val mImage: ImageView = itemView.findViewById(R.id.row_image)

        init {

            itemView.setOnClickListener {
                Toast.makeText(
                        mContext,
                        rowList!![adapterPosition].type,
                        Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): LandingAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.row_item, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        val cardItem = rowList!![position]

        //loading the data
        viewHolder.mType.text = cardItem.type
        viewHolder.mOrderCount.setText(cardItem.orderCount)

        //image loading using Glide
        Glide.with(mContext).load(rowList!![position].image).into(viewHolder.mImage)


    }

    override fun getItemCount(): Int {
        return rowList!!.size
    }
}