package com.vidhyashree.assignment.kotlinmvpdemo.retrofit

import android.content.Context

import java.io.File

import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetroFitAPIClient {
    private const val BASE_URL = "https://dl.dropboxusercontent.com/"
    private var retrofit: Retrofit? = null

    //instance of Retrofit object
    fun getRetrofitInstance(context: Context): Retrofit? {
        if (retrofit == null) {

            //Here a logging interceptor is created
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            //Http cache for offline cache
            val httpCacheDirectory = File(context.cacheDir, "offlineCache")
            val cache = Cache(httpCacheDirectory, (10 * 1024 * 1024).toLong())

            //okhttp interceptor
            val httpClient = OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(interceptor)
                    .build()

            //configuring with base_url
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return retrofit
    }
}
