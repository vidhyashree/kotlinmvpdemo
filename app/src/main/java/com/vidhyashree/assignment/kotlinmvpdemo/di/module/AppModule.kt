package com.vidhyashree.assignment.kotlinmvpdemo.di.module

import android.app.Application
import com.vidhyashree.assignment.kotlinmvpdemo.MyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

//Provides application instance across the project
@Module
class AppModule(private val myApplication: MyApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return myApplication
    }
}
