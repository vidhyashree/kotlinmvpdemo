package com.vidhyashree.assignment.kotlinmvpdemo.mvp.interactor

import com.vidhyashree.assignment.kotlinmvpdemo.model.ListItem

//Model has an interface onFinished which triggers after the handler interval succeeds.
interface Interactor {
    //fetch new data from the server
    fun fetchNewDataFromServer(onFinishedListener: OnFinishedListener)

    interface OnFinishedListener {
        //on request finished
        fun onFinished(item: ListItem)

        //on request failure
        fun onFailure(t: Throwable)

    }


}
