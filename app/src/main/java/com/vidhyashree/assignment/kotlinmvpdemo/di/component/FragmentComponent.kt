package com.vidhyashree.assignment.kotlinmvpdemo.di.component

import com.vidhyashree.assignment.kotlinmvpdemo.view.ListViewFragment
import com.vidhyashree.assignment.kotlinmvpdemo.di.scope.ActivityScope
import com.vidhyashree.assignment.kotlinmvpdemo.di.module.FragmentModule
import dagger.Component

//This Component is used to inject the Presenter in our ListViewFragment.
@ActivityScope
@Component(dependencies = [AppComponent::class], modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(listViewFragment: ListViewFragment)

}
